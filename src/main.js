// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import App from './App'
import i18n from './lang/__i18n'
import router from './router'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faUserSecret, faGlobeAsia, faTimes } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import VueGeolocation from 'vue-browser-geolocation'
import moment from 'moment'
import FormError from './components/FormError'
library.add(faUserSecret, faGlobeAsia, faTimes)

Vue.component('font-awesome-icon', FontAwesomeIcon)
Vue.use(BootstrapVue)
Vue.use(VueGeolocation)
Vue.config.productionTip = false
Vue.prototype.moment = moment
Vue.prototype.$isNull = function (property) {
  return property === undefined || property === null || property === '' || property === {} || property === []
}
Vue.prototype.$getFormat = function (type = null) {
  if (type == null) {
    return localStorage.locale === 'vi' ? 'DD/MM/YYYY' : (localStorage.locale === 'en' ? 'MM/DD/YYYY' : '')
  }
}
Vue.prototype.$valid = function (validObject) {
  let isValid = true
  for (let i = 0; i < validObject.length; i++) {
    let property = validObject[i].property
    if (!this.$checkError(property, validObject[i].error)) isValid = false
  }
  return isValid
}

Vue.prototype.$checkError = function (property, errorObject) {
  for (let j = 0; j < errorObject.length; j++) {
    switch (errorObject[j].type) {
      case 'NULL':
        if (this.$isNull(this[property])) {
          this.$changeState(property, errorObject[j])
          return false
        }
        break
      case 'MAX-LENGTH':
        if (this[property] !== undefined && this[property] !== null && this[property].length > errorObject[j].data) {
          this.$changeState(property, errorObject[j])
          return false
        }
        break
      case 'REGEX':
        if (this.$isNull(this[property])) continue
        if (!errorObject[j].data.test(this[property])) {
          this.$changeState(property, errorObject[j])
          return false
        }
        break
      case 'EMAIL':
        if (this.$isNull(this[property])) continue
        if (!/^[a-z][a-z0-9_.]{5,32}@[a-z0-9]{2,}(\.[a-z0-9]{2,4}){1,2}$/.test(this[property])) {
          this.$changeState(property, errorObject[j])
          return false
        }
        break
      case 'SELECT-OBJECT':
        if (this.$isNull(this[property])) continue
        let val = this[property][errorObject[j].key]
        for (let i = 0; i < errorObject[j].data.length; i++) {
          if (errorObject[j].data[i] === val) {
            this.$changeState(property, errorObject[j])
            return false
          }
        }
        break
      case 'ARRAY-0':
        if (this.$isNull(this[property])) continue
        if (Array.isArray(this[property]) && this[property].length === 0) {
          this.$changeState(property, errorObject[j])
          return false
        }
        break
    }
  }
  this.$changeState(property, {errorDesc: ''}, true)
  return true
}

Vue.prototype.$changeState = function (property, errorObj, type = false, normal = false) {
  let dom = this.$refs[property + 'Parent']
  for (let i = 0; i < dom.childNodes.length; i++) {
    if (dom.childNodes[i].nodeType === Node.ELEMENT_NODE && dom.childNodes[i].id !== property + 'FormErrorID') {
      dom.childNodes[i].classList.remove('border-success')
      dom.childNodes[i].classList.remove('border-invalid')
      if (!normal) dom.childNodes[i].classList.add((type ? 'border-success' : 'border-invalid'))
    } else {
      dom.removeChild(dom.childNodes[i])
      i--
    }
  }
  if ((!normal) && (dom.lastElementChild.id !== property + 'FormErrorID')) {
    let testDiv = document.createElement('div')
    testDiv.setAttribute('id', property + 'FormErrorID')
    dom.appendChild(testDiv)
    const ComponentCtor = Vue.extend(FormError)
    const componentInstance = new ComponentCtor()
    componentInstance.setError(!type)
    let errorDesc = errorObj.errorDesc
    if (errorObj['errorDesc_' + localStorage.locale] !== undefined) errorDesc = errorObj['errorDesc_' + localStorage.locale]
    componentInstance.setErrorDesc(errorDesc)
    componentInstance.setErrorID(property + 'FormErrorID')
    componentInstance.$mount('#' + property + 'FormErrorID')
  }
}
/* eslint-disable no-new */
export const serverBus = new Vue({
  el: '#app',
  i18n,
  router,
  components: { App },
  template: '<App/>'
})
