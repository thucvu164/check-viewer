import Vue from 'vue'
import VueI18n from 'vue-i18n'
import vnMessage from './__vn.json'
import enMessage from './__en.json'

Vue.use(VueI18n)

const messages = {
  vi: vnMessage,
  en: enMessage
}
if (localStorage.locale === undefined) {
  localStorage.locale = 'en'
}

const i18n = new VueI18n({
  locale: localStorage.locale,
  messages,
  fallbackLocale: localStorage.locale
})

export default i18n
